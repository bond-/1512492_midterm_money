//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by 1512492Moneymoney.rc
//
#define IDC_MYICON                      2
#define IDD_1512492MONEYMONEY_DIALOG    102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDC_GROUPBOX                    106
#define IDI_1512492MONEYMONEY           107
#define IDI_SMALL                       108
#define IDC_1512492MONEYMONEY           109
#define IDD_CLEAR_DIALOG                110
#define ID_FILE_NEW                     111
#define ID_FILE_CLEAR                   112
#define IDC_BUTTON_CLOSE                123
#define IDL_LISTVIEW                    124
#define IDR_MAINFRAME                   128
#define IDC_BUTTON_ADD                  1234
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           113
#endif
#endif
